{% macro fastship_incremental_macro(target, source, unique_key, dest_columns) -%}

{%- set dest_columns = dest_columns|reject('in', ['update_date','operation_flag'])|list %}

UPDATE {{ target }}
SET UPDATE_DATE = to_char(now(), 'YY-MM-DD--HH24:MI:SS'),
	OPERATION_FLAG = 'D'
WHERE (
  {% for uk in unique_key -%}
  {{ uk }},
{%- endfor -%} 'a')
IN (
SELECT
{% for uk in unique_key -%}
  T.{{ uk }},
{%- endfor -%} 'a'
  FROM {{ target }} T
  LEFT JOIN {{ source }} S
  ON 1=1
  {%- for uk in unique_key %}
    AND S.{{ uk }} = T.{{ uk }}
  {%- endfor %}
  WHERE S.OPERATION_FLAG IS NULL
  )
;

INSERT INTO {{ target }}
SELECT
  {%- for dc in dest_columns %}
    S.{{ dc }},
  {%- endfor %}
  to_char(now(), 'YY-MM-DD--HH24:MI:SS') AS UPDATE_DATE,
  'I' AS OPERATION_FLAG
  FROM {{ source }} S
  LEFT JOIN {{ target }} T
  ON 1=1
  {%- for uk in unique_key %}
    AND S.{{ uk }} = T.{{ uk }}
  {%- endfor %}
  WHERE T.OPERATION_FLAG IS NULL
;

UPDATE {{ target }} T
SET
{%- for dc in dest_columns %}
  {{ dc }} = S.{{ dc }},
{%- endfor %}
UPDATE_DATE = to_char(now(), 'YY-MM-DD--HH24:MI:SS'),
OPERATION_FLAG = 'U'
FROM {{ source }} S
WHERE 1=1
{%- for uk in unique_key %}
  AND S.{{ uk }} = T.{{ uk }}
{%- endfor %}
AND (1=0
  {%- for dc in dest_columns %}
    or T.{{ dc }} <> S.{{ dc }}
  {%- endfor %}
  )

{%- endmacro %}

{% materialization fastship_delta, default -%}

  {% set unique_key = config.get('unique_key') %}
  {% set full_refresh_mode = flags.FULL_REFRESH %}

  {% set target_relation = this %}
  {% set existing_relation = load_relation(this) %}
  {% set tmp_relation = make_temp_relation(this) %}

  {{ run_hooks(pre_hooks, inside_transaction=False) }}

  -- `BEGIN` happens here:
  {{ run_hooks(pre_hooks, inside_transaction=True) }}

  {% set to_drop = [] %}
  {% if existing_relation is none %}
      {% set build_sql = create_table_as(False, target_relation, sql) %}
  {% elif existing_relation.is_view or full_refresh_mode %}
      {#-- Make sure the backup doesn't exist so we don't encounter issues with the rename below #}
      {% set backup_identifier = existing_relation.identifier ~ "__dbt_backup" %}
      {% set backup_relation = existing_relation.incorporate(path={"identifier": backup_identifier}) %}
      {% do adapter.drop_relation(backup_relation) %}

      {% do adapter.rename_relation(target_relation, backup_relation) %}
      {% set build_sql = create_table_as(False, target_relation, sql) %}
      {% do to_drop.append(backup_relation) %}
  {% else %}
      {% set tmp_relation = make_temp_relation(target_relation) %}
      {% do run_query(create_table_as(True, tmp_relation, sql)) %}
      {% do adapter.expand_target_column_types(
             from_relation=tmp_relation,
             to_relation=target_relation) %}
      {% set dest_columns=get_columns_in_relation(target_relation)|map(attribute='name')|list %}
      {% set build_sql = fastship_extras_package.fastship_incremental_macro(target_relation, tmp_relation, unique_key=unique_key, dest_columns=dest_columns) %}
  {% endif %}

  {% call statement("main") %}
      {{ build_sql }}
  {% endcall %}

  {{ run_hooks(post_hooks, inside_transaction=True) }}

  -- `COMMIT` happens here
  {% do adapter.commit() %}

  {% for rel in to_drop %}
      {% do adapter.drop_relation(rel) %}
  {% endfor %}

  {{ run_hooks(post_hooks, inside_transaction=False) }}

  {{ return({'relations': [target_relation]}) }}

{%- endmaterialization %}
